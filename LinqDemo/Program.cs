﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqDemo
{



    static class Program
    {
        static void Main(string[] args)
        {
            using (NWDataContext nw = new NWDataContext())
            {

                nw.Log = Console.Out;

                var products = nw.Products;
                var categories = nw.Categories;
                var customers = nw.Customers;
                var employees = nw.Employees;

                // SELECT

                var qq0 = from p in products
                          select new { p.ProductName, p.UnitPrice };

                var qa0 = products
                            .Select(p => new { p.ProductName, p.UnitPrice });

                // WHERE

                var qq1 = from p in products
                          where p.UnitPrice < 20
                          select p.ProductName;
                var qa1 = products
                            .Where(p => p.UnitPrice < 20)
                            .Select(p => p.ProductName);

                // ORDER BY and DESC

                var qq2 = from p in products
                          where p.UnitPrice > 20
                          orderby p.CategoryID descending, p.ProductName
                          select new { p.CategoryID, p.ProductName, p.UnitPrice };

                var qa2 = products
                            .Where(p => p.UnitPrice > 20)
                            .OrderByDescending(p => p.CategoryID)
                            .ThenBy(p => p.ProductName)
                            .Select(p => new { p.CategoryID, p.ProductName, p.UnitPrice });

                // DISTINCT

                var qq3 = (
                            from c in customers
                            select c.Country
                           )
                           .Distinct();

                var qa3 = customers
                            .Select(c => c.Country)
                            .Distinct();

                // JOIN

                var qq4 = from p in products
                          join c in categories on p.CategoryID equals c.CategoryID
                          select new { c.CategoryName, p.ProductName, p.UnitPrice };

                var qa4 = products
                            .Join(
                                    categories,
                                    p => p.CategoryID,
                                    c => c.CategoryID,
                                    (p, c) => new { c.CategoryName, p.ProductName, p.UnitPrice }
                                 )
                            ;

                var qq5 = from c in categories
                          join p in products on c.CategoryID equals p.CategoryID into g
                          select new
                          {
                              c.CategoryName,
                              Tooteid = g.Count(),
                              Keskmine = g.Average(x => x.UnitPrice),
                              Laoseis = g.Sum(x => x.UnitPrice * x.UnitsInStock)
                          };

                var qa5 = categories
                            .GroupJoin(
                                products,
                                c => c.CategoryID,
                                p => p.CategoryID,
                                (c, g) => new
                                {
                                    c.CategoryName,
                                    Tooteid = g.Count(),
                                    Keskmine = g.Average(x => x.UnitPrice),
                                    Laoseis = g.Sum(x => x.UnitPrice * x.UnitsInStock)
                                }
                            );


                var qq6 = from e in employees
                          join m in employees on e.ReportsTo equals m.EmployeeID
                          select new
                          {
                              Töötaja = e.FirstName + " " + e.LastName,
                              Ülemus = m.FirstName + " " + m.LastName
                          }
                          ;




                Console.WriteLine(qa5.AsEnumerable().ElementAt(4).CategoryName);
                Console.WriteLine(qa5.AsEnumerable().ElementAtOrDefault(9)?.CategoryName);
                Console.WriteLine(qa5.Where(x => x.Tooteid > 10).First().CategoryName);
                Console.WriteLine(qa5.Where(x => x.Tooteid > 20).FirstOrDefault()?.CategoryName);
                Console.WriteLine(categories.Where(x => x.CategoryID == 8).Single().CategoryName);
                Console.WriteLine(categories.Where(x => x.CategoryID == 9).SingleOrDefault()?.CategoryName);





                // testimiseks


                qq6.Prindi();

            }          
        }

        public static void Prindi(this IEnumerable<object> q)
        {
            foreach (var x in q) Console.WriteLine(x);
            {

            }
        }
    }
}
